#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

my @fh;

for (@ARGV) {
  open my $fh, '<', $_ or die "Unable to open '$_' for reading: $!";
  push @fh, $fh;
}

while (grep { not eof } @fh) {
  for my $fh (@fh) {
    if (defined(my $line = <$fh>)) {
      chomp $line;
      print "$line\n";
    }
  }
}
