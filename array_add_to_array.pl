#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Data::Dumper qw(Dumper);

my @members = ("Time", "Flies");
my @initiates = ("An", "Arrow");
push(@members, @initiates);
#push(a, b) add b to a.
print Dumper \@members;
print join ", ",  @members;