#how to use perl
#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Data::Dumper qw(Dumper);

my %hash1 = (
    "1" => 0,
    "2" => 0,
    "3" => 0,
    "4" => 0,
    "5" => 0,
);

my %hash2 = (
    "2"      => '1',
    "3"      => '1',
    "5"      => '1',
    "result" => '0',
);

#print join ", ", %hash1;
#print "\n";
#print join ", ", %hash2;
#print "\n";
#print join ", ", (%hash1, %hash2);

print Dumper \%hash1;
print Dumper \%hash2;
#print Dumper \(%hash1, %hash2);


@hash1{keys %hash2} = values %hash2;

print Dumper \%hash1;



#print { %hash1, %hash2 };
